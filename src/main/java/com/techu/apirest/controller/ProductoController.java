package com.techu.apirest.controller;

//import com.techu.apirest.service.ProductoModel;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping ("/apitechu/v2")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {

        return productoService.findAll();
  
    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoid(@PathVariable String id) {
//        ProductoModel noProducto = new ProductoModel("000", "null", 0.0);
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

  @PutMapping("/productos")
  public void putProductos(@RequestBody ProductoModel productoToUpdate){
          productoService.save(productoToUpdate);
     }



    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete) {
        return productoService.delete(productoToDelete);
    }


    @PatchMapping ("/ProductoprecioById/{id}")
    public ProductoModel patchEmpleadosCargoById (@RequestBody ProductoModel empleadoToUpdate,@PathVariable String id ){
        if (productoService.findById(id).isPresent()) {
            Optional <ProductoModel> pm = productoService.findById(id);
            if (empleadoToUpdate.getDescripcion() != null) {
                pm.get().setDescripcion (empleadoToUpdate.getDescripcion());
            }
            if (empleadoToUpdate.getPrecio() != null) {
                pm.get().setPrecio (empleadoToUpdate.getPrecio());
            }
            productoService.save(pm.get());
            return pm.get();
        }
        return null;
    }

}
